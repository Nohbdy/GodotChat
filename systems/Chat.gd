extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func host_chat():
	var host = NetworkedMultiplayerENet.new()
	host.create_server(2000)
	get_tree().set_network_peer(host)

func _input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			send_message()

# Join the chat room based on given ip address
func join_room():
	var ip = $ChatWindowPanel/IpInput.text
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, 2000)
	get_tree().set_network_peer(host)

# Send a message into the chat room
func send_message():
	var message = $ChatWindowPanel/ChatPanel/ChatInput.text
	var id = get_tree().get_network_unique_id()
	rpc("receive_message", id, message)

sync func receive_message(id, message):
	$ChatWindowPanel/ChatPanel/ChatBox.text += str(id) + ": " + message + "\n"

func _on_HostRoom_button_up():
	host_chat()


func _on_JoinRoom_button_up():
	join_room()


func _on_Leave_button_up():
	pass # Replace with function body.
